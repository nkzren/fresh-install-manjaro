# fresh-install-arch
Provides initial config for new Manjaro installs

## Setup Timeshift/Other system recovery tool
**DO NOT SKIP THIS**

Trust me, you'll thank me later

## Refresh mirrors
```sh
sudo pacman-mirrors --fasttrack && sudo pacman -Syyu
```

## base-devel && yay
```sh
sudo pacman -S base-devel
*

sudo pacman -S git go
git clone https://aur.archlinux.org/yay.git
cd yay && makepkg -si
cd .. && rm -rf yay/
```

### Install (yay -S \<item\>)
  - terminator
  - visual-studio-code-bin
  - jdk8-openjdk
  - firefox
  - thunderbird
  - franz
  - vlc
  - spotify
  - nodejs
  - npm
  - nvm
  - vim
  - emacs

## ZSH

```sh
yay -S zsh
chsh -s $(which zsh)

# oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

```
##### Nice ZSH Themes
  * theunraveler

## Docker + Docker Compose

```sh
yay -S docker
sudo systemctl start docker.service
sudo systemctl enable docker.service
sudo usermod -aG docker ${USER}
su - ${USER}

yay -S docker-compose
```

## GitHub SSH
```sh
ssh-keygen -t rsa -b 4096 -C "<your_email>"
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
yay -S xclip
xclip -sel clip < ~/.ssh/id_rsa.pub
```

## Dual Boot: Fix clock
```sh
timedatectl set-local-rtc 1
```

## KDE Custom shortcuts
  Import the `custom_shortcuts` file to KDE for these shortcuts (not listed ones are kept on default)
  #### KWin
    * Ctrl+Meta+<ArrowKey> : Navigate virtual desktops
    * Ctrl+Shift+Meta+<ArrowKey> :  Move window to virtual desktops
    * Shift+Meta+<Right> : Window to next screen
    * Shift+Meta+<Left> : Window to previous screen
  
  #### Spectacle/Screenshots
    * PrtScr : Print whole screen
    * Ctrl+PrtScr : Print area
    * Alt+PrtScr : Print current window
